from __future__ import absolute_import

import csdl

from csdl import *

examples = [
    'twitter.content.contains("hello world", cs=True)',
    'twitter.user.location.exists()',
    'interaction.content.contains("hello world")',
    'interaction.type == "twitter"',
    '(interaction.type == "twitter") & (twitter.user.name == "borntyping")',
    'csdl.tags(["hello", "world"], interaction.content.contains, interaction.content.contains_any)',
    'csdl.scores({"ruby": -10,"python": 75,"cdsl": 90,"java": -100})',
]

for python in examples:
    print "Python:", python
    print "Result:\t", str(eval(python)).replace("\n", "\n\t")
    print
