"""CSDL generation library."""

import os.path
import collections

import yaml

__version__ = '0.2.0'
__author__ = 'Sam Clements <sam@borntyping.co.uk>'
__all__ = (
    'Condition', 'Definition', 'InvalidCSDL', 'Return', 'ScoredTag',
    'Statement', 'Tag', 'Target', 'TargetAttribute', 'TargetOperator',
    'interaction', 'scores', 'tags', 'twitter'
)

TARGETS_DIR = os.path.dirname(os.path.realpath(__file__))
TARGETS_PATH = os.path.join(TARGETS_DIR, 'targets.yaml')

with open(TARGETS_PATH, 'r') as f:
    TARGETS = yaml.safe_load(f)


class InvalidCSDL(Exception):
    """Raised in some cases where an operation would generate invalid CSDL."""


class Statement(object):
    """A (valid) CSDL statement."""

    def __init__(self, fmt, *args, **kwargs):
        self.string = fmt.format(*args, **kwargs)

    def __str__(self):
        return self.string

    def __repr__(self):
        return '<{0} \'{1}\'>'.format(self.__class__.__name__, str(self))

    def __eq__(self, other):
        return str(self) == str(other)

    def __contains__(self, other):
        return other in str(self)


class Not(Statement):
    """
    Negates a statement.

    >>> Not(interaction.content.exists())
    <Not 'NOT interaction.content exists'>
    """

    def __init__(self, statement):
        super(Not, self).__init__('NOT {0}', statement)


class Condition(Statement):
    """
    A target, operator, and argument; or multiple conditions.

    >>> interaction.content.exists()
    <Condition 'interaction.content exists'>

    Can be used with logical operators:

    >>> interaction.type.equals('twitter') & interaction.content.exists()
    <Condition 'interaction.type == "twitter" AND interaction.content exists'>
    >>> interaction.content.exists() | interaction.author.exists()
    <Condition 'interaction.content exists OR interaction.author exists'>
    """

    def __and__(self, other):
        return self.__class__('{0} AND {1}', self, other)

    def __or__(self, other):
        return self.__class__('{0} OR {1}', self, other)

    def not_(self):
        return Not(self)


class Group(Condition):
    """
    A statement surrounded by () for better grouping of conditions.

    >>> Group(interaction.content.exists() & interaction.author.exists())
    <Group '( interaction.content exists AND interaction.author exists )'>
    """

    def __init__(self, statement):
        super(Group, self).__init__('( {0} )', statement)


class Return(Statement):
    """A return statement for a CSDL definition."""

    def __init__(self, statement):
        super(Return, self).__init__('return {{ {0} }}', statement)


class Tag(Statement):
    """
    A tag statement for a CSDL definition.

    >>> Tag('datasift', interaction.hashtags.contains('datasift'))
    <Tag 'tag "datasift" { interaction.hashtags contains "datasift" }'>
    """

    def __init__(self, name, statement):
        super(Tag, self).__init__(
            'tag \"{name}\" {{ {statement} }}', name=name, statement=statement)


class Score(Statement):
    """
    A scored tag statement for a CSDL definition.

    >>> Score('namespace.example', interaction.type.equals('twitter'), +10)
    <Score 'tag.namespace.example +10 { interaction.type == "twitter" }'>
    >>> Score('namespace.example', interaction.type.equals('facebook'), -1)
    <Score 'tag.namespace.example -1 { interaction.type == "facebook" }'>
    """

    def __init__(self, name, statement, score):
        super(Score, self).__init__(
            'tag.{name} {score} {{ {statement} }}',
            name=name, statement=statement, score='{0:+}'.format(score))


class Definition(object):
    """
    A CSDL definition, consisting of multiple statements.

    Cannot have more than one Return statement.

    >>> definition = Definition()
    >>> definition << Tag('example', interaction.content.contains('example'))
    >>> definition.returns(interaction.content.contains('example'))
    >>> print(definition)
    tag "example" { interaction.content contains "example" }
    return { interaction.content contains "example" }
    """

    def __init__(self, return_statement=None):
        self.statements = []

        if return_statement:
            self.returns(return_statement)

    def __str__(self):
        return "\n".join(map(str, self.statements))

    def __repr__(self):
        return '<{0} {1}>'.format(self.__class__.__name__, self.statements)

    def prepend(self, statement):
        """Insert a statement before the final statement."""
        self.insert((len(self) - 1) or 0, statement)

    def __lshift__(self, other):
        return self.statements.append(other)

    def tag(self, name, csdl):
        self.statements.prepend(Tag(name, csdl))

    def returns(self, return_statement):
        for statement in self.statements:
            if isinstance(statement, Return):
                raise InvalidCSDL("Cannot have two return statements")
        self.statements.append(Return(return_statement))


class TargetAttribute(object):
    """
    A field used to build Target attributes.

    >>> class Example(Target):
    ...     attribute = TargetAttribute('attribute')
    >>> target = Example('example')
    >>> target.attribute
    <Target 'example.attribute'>
    """

    def __init__(self, _name):
        self._name = _name

    def __get__(self, instance, owner):
        return Target(instance._name + "." + self._name)


class TargetOperator(object):
    """
    An property field used to build Target classes.

    >>> class Example(Target):
    ...     operator = TargetOperator('operator', accepts=str)
    >>> target = Example('example')
    >>> str(target.operator('argument'))
    'example operator "argument"'
    """

    def __init__(self, operator, accepts=None, cs=False):
        self.operator = operator
        self.accepts = self._accepts_types(accepts)
        self.cs = cs

        #: Instances are used as properties and so having their own docstring
        #: is useful. It also means docstring examples aren't repeated.
        self.__doc__ = self._docstring

    @staticmethod
    def _accepts_types(accepts):
        """Return a tuple of types from None, a type, or multiple types."""
        # No arguments
        if accepts is None:
            return tuple()
        # Arguments of one type
        elif isinstance(accepts, type):
            return (accepts,)
        # Arguments of multiple types
        elif isinstance(accepts, collections.Iterable):
            return tuple(accepts)
        raise Exception("accepts must be None, a type or a tuple of types")

    @property
    def _docstring(self):
        """Generate a docstring for a TargetOperator instance."""
        doc = "Operator function for ``{0}``.\n".format(self.operator)

        if self.cs:
            doc += "\nThis operator is case sensitive."

        if self.accepts is not None:
            doc += "\nThis operator accepts arguments of type ``{0}``.".format(
                '``, ``'.join([c.__name__ for c in self.accepts]))

        return doc

    def __get__(self, instance, owner):
        """Return an operator function."""
        if self.cs:
            def operator(argument=None, cs=False):
                return self.condition(instance, argument, cs)
        else:
            def operator(argument=None):
                return self.condition(instance, argument)
        return operator

    def condition(self, target, argument=None, cs=False):
        if cs:
            segments = [target, "cs", self.operator]
        else:
            segments = [target, self.operator]

        if self.accepts:
            # Check that the argument is acceptable
            if not isinstance(argument, self.accepts):
                raise InvalidCSDL("Argument must be {0}".format(self.accepts))

            # Deal with lists
            if isinstance(argument, (list, tuple)):
                argument = ", ".join(argument)

            # String arguments are wrapped with quotes
            if isinstance(argument, str):
                argument = '"' + argument + '"'

            segments.append(argument)

        return Condition(" ".join(map(str, segments)))


class Target(object):
    """
    Identifies a piece of data that CSDL can filter.

    >>> interaction
    <Target 'interaction'>
    >>> interaction.content
    <Target 'interaction.content'>
    """

    @classmethod
    def create(cls, name, data, parent=None):
        attributes = dict()

        for item in data.pop('attributes', []):
            attributes[item] = TargetAttribute(item)

        for key, value in data.pop('subclasses', {}).items():
            attributes[key.lower()] = Target.create(key, value, parent=name)

        # Use the parent's name as a prefix if this is a child target
        clsname = name if parent is None else parent + name

        cls = type(clsname, (cls,), attributes)

        return cls(name.lower())

    def __init__(self, _name):
        self._name = _name

    def __repr__(self):
        return "<Target '{0}'>".format(self._name)

    def __str__(self):
        return self._name

    def __get__(self, instance, owner):
        """Used by subattributes to return self prefixed by the parent."""
        return self.__class__(instance._name + "." + self._name)

    # Operators

    contains = TargetOperator("contains", accepts=str, cs=True)

    substr = TargetOperator("substr", accepts=str, cs=True)

    contains_any = TargetOperator("contains_any", accepts=list, cs=True)
    any = contains_any

    contains_all = TargetOperator("contains_all", accepts=list, cs=True)
    all = contains_all

    exists = TargetOperator("exists")

    _in = TargetOperator("in", accepts=list, cs=True)

    url_in = TargetOperator("url_in", accepts=list, cs=True)

    __eq__ = TargetOperator("==", accepts=(int, float, str), cs=True)
    equals = __eq__

    __ne__ = TargetOperator("!=", accepts=(int, float, str), cs=True)
    notequal = __ne__

    regex_partial = TargetOperator("regex_partial", accepts=str)

    regex_exact = TargetOperator("regex_exact", accepts=str)

#: Interaction is the parent of all other targets
interaction = Target.create("Interaction", TARGETS.pop('Interaction'))

for _name, _attributes in TARGETS.items():
    globals()[_name.lower()] = interaction.create(_name, _attributes)


def tags(
        tags,
        tag_csdl=interaction.content.contains,
        returns_csdl=interaction.content.contains_any):
    """Create a simple CSDL tag filter."""
    result = Definition()

    for tag in tags:
        result << Tag(tag, tag_csdl(tag))

    if returns_csdl is not None:
        result.returns(returns_csdl(tags))

    return result


def scores(
        tags,
        match_csdl=interaction.content.contains,
        returns_csdl=interaction.content.contains_any):
    """Create a simple CSDL scores filter."""
    result = Definition()

    for tag, score in tags.items():
        result << Score(tag, match_csdl(tag), score)

    if returns_csdl is not None:
        result.returns(returns_csdl(tags.keys()))

    return result
