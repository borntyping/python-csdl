from __future__ import print_function

from csdl import interaction, links
from csdl import Group, Statement


def test_keywords_example():
    keywords = ['csdl', 'datasift']
    assert isinstance(Group(
        interaction.raw_content.contains_any(keywords) |
        interaction.hashtags.contains_any(keywords) |
        interaction.title.contains_any(keywords) |
        interaction.author.name.contains_any(keywords) |
        interaction.author.username.contains_any(keywords) |
        links.url.contains_any(keywords) |
        links.domain.contains_any(keywords)
    ), Statement)
