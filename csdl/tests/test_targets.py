import pytest

from csdl import interaction, twitter, Target, Condition, InvalidCSDL


def test_target_class():
    assert isinstance(interaction.content, Target)


def test_target_subclass():
    assert isinstance(interaction.author.id, Target)


def test_twitter_content():
    assert twitter.content == "twitter.content"


def test_twitter_text_exists():
    assert twitter.content.exists() == "twitter.content exists"


def test_twitter_content_contains():
    assert twitter.content.contains("hello world") == (
        "twitter.content contains \"hello world\"")


def test_twitter_content_contains_invalid():
    with pytest.raises(InvalidCSDL):
        twitter.content.contains(-1)


def test_twitter_content_content_invalid():
    with pytest.raises(AttributeError):
        twitter.content.content


def test_twitter_content_contains_type():
    csdl = twitter.content.contains("hello world")
    assert csdl.__class__ is Condition


def test_twitter_user_description_exists():
    assert twitter.user.url.exists() == "twitter.user.url exists"


def test_interaction_cs_containts():
    assert "cs" in interaction.content.contains("hello world", cs=True)


def test_interaction_cs_exists_invalid():
    with pytest.raises(TypeError):
        assert "cs" in interaction.content.exists(cs=True)


def test_target_operators():
    interaction.content.contains("hello world")
    interaction.content.contains_any(["hello", "world"])
    interaction.content.any(["hello", "world"])
    interaction.content.contains_all(["hello", "world"])
    interaction.content.all(["hello", "world"])
    interaction.type._in(["twitter", "linkedin"])
    interaction.type.url_in(["www.apple.com", "www.google.com"])
    interaction.type == "string"
