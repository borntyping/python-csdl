python-csdl
===========

.. image:: http://img.shields.io/pypi/v/csdl-unoffical.svg?style=flat-square
    :target: https://pypi.python.org/pypi/csdl-unoffical
    :alt: csdl-unoffical on PyPI

.. image:: http://img.shields.io/pypi/l/csdl-unoffical.svg?style=flat-square
    :target: https://pypi.python.org/pypi/csdl-unoffical
    :alt: csdl-unoffical on PyPI

.. image:: http://img.shields.io/travis/borntyping/python-csdl/master.svg?style=flat-square
    :target: https://travis-ci.org/borntyping/python-csdl
    :alt: Travis-CI build status for python-csdl

.. image:: https://img.shields.io/github/issues/borntyping/python-csdl.svg?style=flat-square
    :target: https://github.com/borntyping/python-csdl/issues
    :alt: GitHub issues for python-csdl

|

The ``csdl`` library lets you write CSDL_ inside Python.

**This project is incomplete and not actively maintained, and is not supported by DataSift.**

* `Source on GitHub <https://github.com/borntyping/python-csdl>`_
* `Packages on PyPI <https://pypi.python.org/pypi/csdl-unoffical>`_
* `Builds on Travis CI <https://travis-ci.org/borntyping/python-csdl>`_


Example
-------

Thw following python script...

.. code-block:: python

    from csdl import interaction, twitter, Definition

    print (interaction.type == "twitter") & (twitter.user.name == "borntyping")

...results in this output.

.. code::

    interaction.type == "twitter" AND twitter.user.name == "borntyping"

Installation
------------

Install using pip_:

.. code-block:: bash

    pip install csdl-unoffical

Authors
-------

The ``csdl`` module was written by `Sam Clements`_ during a DataSift_ hackday. It is not offically supported by DataSift_.

.. _CSDL: http://dev.datasift.com/docs/csdl
.. _pip: http://pip.readthedocs.org/en/stable/
.. _Sam Clements: https://github.com/borntyping
.. _DataSift: http://datasift.com/
