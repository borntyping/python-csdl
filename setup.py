from setuptools import setup

setup(
    name='csdl-unoffical',
    version='0.3.0',

    description='Write CSDL using Python',
    long_description=open('README.rst').read(),

    author='Sam Clements',
    author_email='sam@borntyping.co.uk',
    url='https://github.com/borntyping/python-csdl',
    license='MIT License',

    packages=[
        'csdl'
    ],

    package_data={
        'csdl': [
            'targets.yaml'
        ]
    },

    install_requires=[
        'PyYAML'
    ],

    classifiers=[
        'Development Status :: 4 - Beta',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4'
    ]
)
