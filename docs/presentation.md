title: CSDL in Python
output: README.html
author:
    url: https://github.com/borntyping/python-csdl
controls: true

--

# CSDL in Python
## AKA: I wish I had a cool name for this

--

### Writing CSDL with Python

The `csdl` library lets you write CSDL inside Python.

For example:

    from csdl import interaction, twitter, Definition

    print (interaction.type == "twitter") & (twitter.user.name == "borntyping")

Results in:

    interaction.type == "twitter" AND twitter.user.name == "borntyping"

--

### Writing dynamic CSDL with Python

That's quite verbose, and the result is shorter than the script.
But this can be used alongside other python constructs and control flow.

    from csdl import Definition, Tag, interaction

    def tags(list):
        result = Definition()

        for tag in list:
            result << Tag(tag, interaction.content.contains(tag))

        result.returns(interaction.content.contains_any(tags))

        return result

--

### Generating tags

Using this function, we can quickly generate a definition containing a large number of tags:

    print tags(["java", "ruby", "python", "c++", "lisp", "php", "go"])

Results in:

    tag "java" { interaction.content contains "java" }
    tag "ruby" { interaction.content contains "ruby" }
    tag "python" { interaction.content contains "python" }
    tag "c++" { interaction.content contains "c++" }
    tag "lisp" { interaction.content contains "lisp" }
    tag "php" { interaction.content contains "php" }
    tag "go" { interaction.content contains "go" }
    return { interaction.content contains_any "java, ruby, python, c++, lisp, php, go" }

--

### Scoring tags

The `tags` function has a similar counterpart, the `score` function, which makes scoring a set of tags really simple:

    from csdl import scores

    print scores({"ruby": -10, "python": 75, "cdsl": 90, "java": -100})

Results in:

    tag.python 75 { interaction.content contains "python" }
    tag.cdsl 90 { interaction.content contains "cdsl" }
    tag.ruby -10 { interaction.content contains "ruby" }
    tag.java -100 { interaction.content contains "java" }
    return { interaction.content contains_any "python, cdsl, ruby, java" }

--

### Using external data

Instead of using hardcoded values, data can easily be read from an external source and used to create a definition:

`tags.json`

    {"ruby": -10, "python": 75, "cdsl": 90, "java": -100}

`example.py`

    with open("tags.json") as file:
        print scores(json.loads(file.read()))
